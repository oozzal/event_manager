package lib;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Library to send E-Mail<br><br>
 *
 * <b>Usage:</b><br>
 * String user = {"theoozzal@gmail.com"};<br>
 * String pass = "password";<br>
 * String[] recipients = {"hello@company.com", "oozzal@yahoo.com"};<br>
 * String subject = "Alert";<br>
 * String body = "You have subscribed";<br>
 * new Thread(new Emailer(user, pass, recipients, subject, body)).start();<br>
 *
 * @author uzzaldevkota
 */
public class Emailer implements Runnable {

    private final String username, password;
    private final String[] recipients;
    private final String subject, body;

    /**
     * Send Email to a list of recipients
     *
     * @param user String Sender email address
     * @param pass String Password of sender
     * @param to String[] Recipients email addresses
     * @param sub String The subject of the email
     * @param content String The e-mail body
     */
    public Emailer(String user, String pass, String[] to, String sub, String content) {
        username = user;
        password = pass;
        recipients = to;
        subject = sub;
        body = content;
    }

    private Properties getProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        return props;
    }

    @Override
    public void run() {
        System.out.println("Sending...");
        Properties props = getProperties();
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            for (String recipient : recipients) {
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            }
            message.setSubject(subject);
            message.setText(body);
            Transport.send(message);
            System.out.println("Sent!");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
