package event_manager.controllers;

import event_manager.views.View;
import event_manager.views.View;
import org.javalite.activejdbc.Model;

/**
 * General interface for application controllers
 * @author uzzaldevkota
 */
public interface Controller {

    public void index(View view);

    public void createNew(View view);

    public boolean save(View view, Model model);

    public void show(View view, int modelId);

    public void edit(View view, int modelId);

    public boolean delete(View view, int modelId);

}
