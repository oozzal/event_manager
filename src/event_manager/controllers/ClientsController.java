package event_manager.controllers;

import event_manager.helpers.DBHelper;
import event_manager.helpers.ViewHelper;
import event_manager.models.Client;
import event_manager.views.Show;
import event_manager.views.View;
import event_manager.views.forms.ClientForm;
import org.javalite.activejdbc.Model;

/**
 * Controller to handle user related actions
 * @author uzzaldevkota
 */
public class ClientsController implements Controller {

    @Override
    public void index(View view) {
        String panelName = "List " + Client.class.getSimpleName();
        view.show(panelName, ViewHelper.LIST_CLIENT);
    }

    @Override
    public void createNew(View view) {
        new ClientForm(view, true, new Client()).setVisible(true);
    }

    @Override
    public boolean save(View view, Model model) {
        DBHelper.openConnection();

        boolean saved = false;
        String errors = "";
        Client client = (Client) model;

        if(client.save()) {
            saved = true;
        } else {
            errors += DBHelper.getNiceErrors(model);
        }

        if (errors.equals("")) {
            ViewHelper.showDialog("Saved!");
        } else {
            ViewHelper.showDialog("Error saving!\n\nErrors:\n"+errors);
        }
        DBHelper.closeConnection();
        return saved;
    }

    @Override
    public void show(View view, int modelId) {
        DBHelper.openConnection();
        Client client = Client.findById(modelId);
        new Show(view, true, client).setVisible(true);
        DBHelper.closeConnection();
    }
    
    @Override
    public void edit(View view, int modelId) {
        DBHelper.openConnection();
        Client client = Client.findById(modelId);
        new ClientForm(view, true, client).setVisible(true);
        DBHelper.closeConnection();
    }

    @Override
    public boolean delete(View view, int modelId) {
        boolean deleted = false;
        DBHelper.openConnection();
        Client client = Client.findById(modelId);
        if(client.delete()) {
            ViewHelper.showDialog("Deleted!");
            deleted = true;
        } else {
            ViewHelper.showDialog("Error deleting!");
        }
        DBHelper.closeConnection();
        return deleted;
    }
}
