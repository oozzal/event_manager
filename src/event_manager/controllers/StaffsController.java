package event_manager.controllers;

import event_manager.helpers.DBHelper;
import event_manager.helpers.FileHelper;
import event_manager.helpers.ViewHelper;
import event_manager.models.Staff;
import event_manager.views.Show;
import event_manager.views.forms.StaffForm;
import event_manager.views.View;
import java.io.File;
import java.io.IOException;
import javax.swing.JFrame;
import org.apache.commons.io.FileUtils;
import org.javalite.activejdbc.Model;

/**
 * Controller to handle user related actions
 * @author uzzaldevkota
 */
public class StaffsController implements Controller {

    @Override
    public void index(View view) {
        String panelName = "List " + Staff.class.getSimpleName();
        view.show(panelName, ViewHelper.LIST_STAFF);
    }

    @Override
    public void createNew(View view) {
        new StaffForm(view, true, new Staff()).setVisible(true);
    }

    @Override
    public boolean save(View view, Model model) {
        DBHelper.openConnection();

        boolean saved = false;
        String errors = "";
        Staff st = (Staff) model;
        String profilePicturePath = st.getProfilePicturePath();

        if(st.save()) {
            saved = true;
            // Upload profile picture if provided
            if(!profilePicturePath.equals("")) {
                File tmpFile = new File(profilePicturePath);
                if(tmpFile.exists() && tmpFile.isFile()) {
                    if(FileHelper.fileSizeInMB(tmpFile) < FileHelper.UPLOAD_LIMIT) {
                        try {
                            String profilePicture = FileHelper.absolutePathToLocalFilename(profilePicturePath, (String)st.get("username"));
                            FileUtils.copyFile(new File(profilePicturePath), new File(FileHelper.PROFILE_PIC_DIR + profilePicture));
                            st.set("profile_picture", profilePicture);
                            st.save();
                        } catch (IOException ex) {
                            errors += "Error saving profile picture";
                        }
                    } else {
                        errors += "The upload limit is " + FileHelper.UPLOAD_LIMIT + "MB";
                    }
                } else {
                    errors += "Invalid File!";
                }
            }
        } else {
            errors += DBHelper.getNiceErrors(model);
        }

        if (errors.equals("")) {
            ViewHelper.showDialog("Saved!");
        } else {
            ViewHelper.showDialog("Error saving!\n\nErrors:\n"+errors);
        }
        DBHelper.closeConnection();
        return saved;
    }

    @Override
    public void show(View view, int modelId) {
        DBHelper.openConnection();
        Staff staff = Staff.findById(modelId);
        new Show(view, true, staff).setVisible(true);
        DBHelper.closeConnection();
    }
    
    @Override
    public void edit(View view, int modelId) {
        DBHelper.openConnection();
        Staff staff = Staff.findById(modelId);
        new StaffForm(view, true, staff).setVisible(true);
        DBHelper.closeConnection();
    }

    @Override
    public boolean delete(View view, int modelId) {
        boolean deleted = false;
        DBHelper.openConnection();
        Staff staff = Staff.findById(modelId);
        if(staff.delete()) {
            ViewHelper.showDialog("Deleted!");
            deleted = true;
        } else {
            ViewHelper.showDialog("Error deleting!");
        }
        DBHelper.closeConnection();
        return deleted;
    }

    /**
     * JFrame only in this method, because View is not exposed during login
     * @param v JFrame
     * @param staff Staff
     */
    public static void login(JFrame v, Staff staff) {
        try {
            DBHelper.openConnection();
            if(staff.isAdmin()) {
                v.dispose();
                new View().maximize().setVisible(true);
            } else {
                ViewHelper.showDialog("Invalid Login");
            }
        } finally {
            DBHelper.closeConnection();
        }
    }

}
