package event_manager.controllers;

import event_manager.helpers.DBHelper;
import event_manager.helpers.ViewHelper;
import event_manager.models.Event;
import event_manager.views.ShowEvent;
import event_manager.views.View;
import event_manager.views.forms.EventForm;
import org.javalite.activejdbc.Model;

/**
 * Controller to handle user related actions
 * @author uzzaldevkota
 */
public class EventsController implements Controller {

    @Override
    public void index(View view) {
        String panelName = "List " + Event.class.getSimpleName();
        view.show(panelName, ViewHelper.LIST_EVENT);
    }

    @Override
    public void createNew(View view) {
        new EventForm(view, true, new Event()).setVisible(true);
    }

    @Override
    public boolean save(View view, Model model) {
        DBHelper.openConnection();

        boolean saved = false;
        String errors = "";
        Event event = (Event) model;
        double vatPercentage = event.getDouble("vat_percentage");
        double totalAmount = event.getDouble("total_amount");
        double amountWithVat = totalAmount + vatPercentage/100 * totalAmount;
        event.set("amount_with_vat", amountWithVat);

        if(event.save()) {
            saved = true;
        } else {
            errors += DBHelper.getNiceErrors(model);
        }

        if (saved) {
            ViewHelper.showDialog("Saved!");
        } else {
            ViewHelper.showDialog("Error saving!\n\nErrors:\n"+errors);
        }
        DBHelper.closeConnection();
        return saved;
    }

    @Override
    public void show(View view, int modelId) {
        DBHelper.openConnection();
        Event event = Event.findById(modelId);
        new ShowEvent(view, true, event).setVisible(true);
        DBHelper.closeConnection();
    }
    
    @Override
    public void edit(View view, int modelId) {
        DBHelper.openConnection();
        Event event = Event.findById(modelId);
        new EventForm(view, true, event).setVisible(true);
        DBHelper.closeConnection();
    }

    @Override
    public boolean delete(View view, int modelId) {
        boolean deleted = false;
        DBHelper.openConnection();
        Event event = Event.findById(modelId);
        if(event.delete()) {
            ViewHelper.showDialog("Deleted!");
            deleted = true;
        } else {
            ViewHelper.showDialog("Error deleting!");
        }
        DBHelper.closeConnection();
        return deleted;
    }
}
