package event_manager.views;

/**
 *
 * @author uzzaldevkota
 */
public interface ViewInterface {
    public void show(String panelName, int panelId);
}
