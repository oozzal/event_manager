package event_manager.views.forms;

import event_manager.views.AppDialog;
import event_manager.controllers.EventsController;
import event_manager.helpers.FormHelper;
import event_manager.helpers.ValidationHelper;
import event_manager.listeners.InputBindingListener;
import event_manager.listeners.LoggingBindingListener;
import event_manager.models.Client;
import event_manager.models.Event;
import event_manager.validators.MinimumLengthValidator;
import event_manager.validators.NumberValidator;
import event_manager.validators.PhoneNumberValidator;
import event_manager.views.View;
import java.sql.Time;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JTextField;
import lib.TransferFocus;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

/**
 *
 * @author uzzaldevkota
 */
public class EventForm extends AppDialog {
    private Event event = new Event();
    private Client client = new Client();
    private String formTitle = "";
    private BindingGroup bindingGroup;
    private MinimumLengthValidator minimumLengthValidator;
    private NumberValidator numberValidator;
    private final View view;

    /**
     * Creates new form EventAppDialog
     * @param view View
     * @param modal boolean
     * @param e Event
     */
    public EventForm(View view, boolean modal, Event e) {
        super(view, modal);
        this.view = view;
        event = e;
        formTitle = (event.get("id") == null ? "Create" : "Edit") + " Event";
        initComponents();
        // :(
        initBindings();
        initFields();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initFields() {
        Date date = (Date) timeSpinner.getValue();
        if(event.get("time") == null) {
            timeField.setText(new Time(date.getTime()).toString());
        } else {
            timeField.setText(event.get("time").toString());
        }
        advanceField.setText("0");
        noOfLaboursField.setText("0");
        noOfVehicleTripsField.setText("0");
        extraExpenseField.setText("0");
        commissionField.setText("0");
        vatPercentageField.setText("0");
    }
    
    /**
     * Returns <code>Event</code> being edited.
     * 
     * @return <code>Event</code> being edited.
     */
    public Event getEvent() {
        return event;
    }

    /** 
     * Sets event to edit.
     * 
     * @param event event to return
     */
    public void setEvent(Event event) {
        Event oldEvent = this.event;
        this.event = event;
        firePropertyChange("event", oldEvent, event);
    }

    private void initBindings() {
        bindingGroup = new BindingGroup();
        minimumLengthValidator = new MinimumLengthValidator(5);
        numberValidator = new NumberValidator();

        Binding binding = FormHelper.createBinding(this, "event", "name", nameField, "text");
        binding.setValidator(minimumLengthValidator);
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "location", locationField, "text");
        binding.setValidator(minimumLengthValidator);
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "description", descriptionTextArea, "text");
        binding.setValidator(minimumLengthValidator);
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "phoneNo", phoneNumberField, "text");
        binding.setValidator(new PhoneNumberValidator());
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "startDate", startDatePicker, "date");
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "endDate", endDatePicker, "date");
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "totalAmount", totalAmountField, "text");
        binding.setValidator(numberValidator);
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "advance", advanceField, "text");
        binding.setValidator(numberValidator);
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "noOfLabours", noOfLaboursField, "text");
        binding.setValidator(numberValidator);
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "noOfVehicleTrips", noOfVehicleTripsField, "text");
        binding.setValidator(numberValidator);
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "extraExpense", extraExpenseField, "text");
        binding.setValidator(numberValidator);
        bindingGroup.addBinding(binding);

        binding = FormHelper.createBinding(this, "event", "commission", commissionField, "text");
        binding.setValidator(numberValidator);
        bindingGroup.addBinding(binding);
        
        binding = FormHelper.createBinding(this, "event", "vatPercentage", vatPercentageField, "text");
        binding.setValidator(numberValidator);
        bindingGroup.addBinding(binding);

        // Error when event is new (createNew)
        if(event.get("paid") != null) {
            binding = FormHelper.createBinding(this, "event", "paid", paidCheckBox, "selected");
            bindingGroup.addBinding(binding);
        }

        bindingGroup.bind();
        for (Binding b : bindingGroup.getBindings()) {
            if (b.getTargetObject() instanceof JTextField) {
                b.addBindingListener(new InputBindingListener((JTextField)b.getTargetObject()));
            }
        }
        bindingGroup.addBindingListener(new LoggingBindingListener(noticeLabel));
    }

    private void submit() {
        client = (Client) clientComboBox.getSelectedItem();
        event.setClientId(client.get("id"));
        event.setTime(timeField.getText());
        if(ValidationHelper.validate(bindingGroup) && new EventsController().save(view, event)) {
            dispose();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
 regenerated by the AppDialog Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        staffFormPanel = new javax.swing.JPanel();
        nameLabel = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        locationLabel = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();
        descriptionLabel = new javax.swing.JLabel();
        phoneNumberLabel = new javax.swing.JLabel();
        phoneNumberField = new javax.swing.JTextField();
        startDateLabel = new javax.swing.JLabel();
        paidCheckBox = new javax.swing.JCheckBox();
        submitButton = new javax.swing.JButton();
        startDatePicker = new org.jdesktop.swingx.JXDatePicker();
        clientComboBox = new javax.swing.JComboBox(Client.getComboBoxModel());
        clientLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        descriptionTextArea = new javax.swing.JTextArea();
        endDateLabel = new javax.swing.JLabel();
        endDatePicker = new org.jdesktop.swingx.JXDatePicker();
        totalAmountLabel = new javax.swing.JLabel();
        totalAmountField = new javax.swing.JTextField();
        advanceLabel = new javax.swing.JLabel();
        advanceField = new javax.swing.JTextField();
        noOfLaboursLabel = new javax.swing.JLabel();
        noOfVehicleTripsLabel = new javax.swing.JLabel();
        noOfLaboursField = new javax.swing.JTextField();
        noOfVehicleTripsField = new javax.swing.JTextField();
        extraExpenseLabel = new javax.swing.JLabel();
        extraExpenseField = new javax.swing.JTextField();
        commissionLabel = new javax.swing.JLabel();
        commissionField = new javax.swing.JTextField();
        extraExpenseLabel1 = new javax.swing.JLabel();
        vatPercentageField = new javax.swing.JTextField();
        timeSpinner = new javax.swing.JSpinner(new javax.swing.SpinnerDateModel());
        timeField = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        locationField = new javax.swing.JTextField();
        noticeLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        staffFormPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, formTitle, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Abadi MT Condensed Extra Bold", 0, 18))); // NOI18N

        nameLabel.setText("Name:");

        locationLabel.setText("Location:");

        timeLabel.setText("Time:");

        descriptionLabel.setText("Description:");

        phoneNumberLabel.setText("Phone No:");

        startDateLabel.setText("Start Date:");

        paidCheckBox.setText("Paid");
        paidCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paidCheckBoxActionPerformed(evt);
            }
        });

        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        startDatePicker.setToolTipText("");

        clientComboBox.setRenderer(Client.getListCellRenderer());

        clientLabel.setText("Client:");

        TransferFocus.patch(descriptionTextArea);
        descriptionTextArea.setColumns(20);
        descriptionTextArea.setRows(5);
        jScrollPane1.setViewportView(descriptionTextArea);

        endDateLabel.setText("End Date:");

        endDatePicker.setToolTipText("");

        totalAmountLabel.setText("Total Amount:");

        advanceLabel.setText("Advance:");

        noOfLaboursLabel.setText("No of Labours:");

        noOfVehicleTripsLabel.setText("No of Vehicle Trips:");

        extraExpenseLabel.setText("Extra Expense:");

        extraExpenseField.setText("0");

        commissionLabel.setText("Commission:");

        commissionField.setText("0");

        extraExpenseLabel1.setText("VAT Percentage:");

        vatPercentageField.setText("0");

        javax.swing.JSpinner.DateEditor timeEditor = new javax.swing.JSpinner.DateEditor(timeSpinner, "HH:mm");
        timeSpinner.setEditor(timeEditor);
        timeSpinner.setValue(new Date());
        timeSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                timeSpinnerStateChanged(evt);
            }
        });

        timeField.setEditable(false);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout staffFormPanelLayout = new javax.swing.GroupLayout(staffFormPanel);
        staffFormPanel.setLayout(staffFormPanelLayout);
        staffFormPanelLayout.setHorizontalGroup(
            staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(staffFormPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(staffFormPanelLayout.createSequentialGroup()
                        .addComponent(submitButton)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(staffFormPanelLayout.createSequentialGroup()
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(clientLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(clientComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(36, 36, 36))
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nameLabel)
                                    .addComponent(nameField))
                                .addGap(22, 22, 22))
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                                    .addComponent(descriptionLabel)
                                    .addComponent(totalAmountLabel)
                                    .addComponent(totalAmountField, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(startDateLabel)
                                    .addComponent(startDatePicker, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                                    .addComponent(noOfLaboursLabel)
                                    .addComponent(noOfLaboursField, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                                    .addComponent(extraExpenseLabel)
                                    .addComponent(extraExpenseField, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                                    .addComponent(extraExpenseLabel1)
                                    .addComponent(vatPercentageField, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE))
                                .addGap(22, 22, 22)))
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(noOfVehicleTripsField)
                            .addComponent(phoneNumberField)
                            .addComponent(endDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(advanceField)
                            .addComponent(commissionField)
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(timeField, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(timeSpinner, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(locationLabel)
                                    .addComponent(paidCheckBox)
                                    .addComponent(commissionLabel)
                                    .addComponent(noOfVehicleTripsLabel)
                                    .addComponent(phoneNumberLabel)
                                    .addComponent(endDateLabel)
                                    .addComponent(advanceLabel)
                                    .addComponent(timeLabel))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(locationField))))
                .addContainerGap())
        );
        staffFormPanelLayout.setVerticalGroup(
            staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(staffFormPanelLayout.createSequentialGroup()
                .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(staffFormPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(clientComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(clientLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(nameLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(locationLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(locationField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(phoneNumberLabel)
                            .addComponent(descriptionLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(phoneNumberField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(timeLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(timeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(timeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(startDateLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(startDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(totalAmountLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(totalAmountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(endDateLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(endDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(advanceLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(advanceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(noOfLaboursLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(noOfLaboursField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(noOfVehicleTripsLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(noOfVehicleTripsField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(staffFormPanelLayout.createSequentialGroup()
                                .addComponent(extraExpenseLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(extraExpenseField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, staffFormPanelLayout.createSequentialGroup()
                                .addComponent(commissionLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(commissionField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(extraExpenseLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(staffFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(vatPercentageField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(paidCheckBox)))
                    .addComponent(jSeparator1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(submitButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        noticeLabel.setText(" ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(staffFormPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(noticeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(staffFormPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(noticeLabel)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        submit();
    }//GEN-LAST:event_submitButtonActionPerformed

    private void paidCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paidCheckBoxActionPerformed
        // Weird :( due to error in binding for new event :(
        event.setPaid(paidCheckBox.isSelected());
    }//GEN-LAST:event_paidCheckBoxActionPerformed

    private void timeSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_timeSpinnerStateChanged
        Date date = (Date) timeSpinner.getValue();
        timeField.setText(new Time(date.getTime()).toString());
    }//GEN-LAST:event_timeSpinnerStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField advanceField;
    private javax.swing.JLabel advanceLabel;
    private javax.swing.JComboBox clientComboBox;
    private javax.swing.JLabel clientLabel;
    private javax.swing.JTextField commissionField;
    private javax.swing.JLabel commissionLabel;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JTextArea descriptionTextArea;
    private javax.swing.JLabel endDateLabel;
    private org.jdesktop.swingx.JXDatePicker endDatePicker;
    private javax.swing.JTextField extraExpenseField;
    private javax.swing.JLabel extraExpenseLabel;
    private javax.swing.JLabel extraExpenseLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField locationField;
    private javax.swing.JLabel locationLabel;
    private javax.swing.JTextField nameField;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JTextField noOfLaboursField;
    private javax.swing.JLabel noOfLaboursLabel;
    private javax.swing.JTextField noOfVehicleTripsField;
    private javax.swing.JLabel noOfVehicleTripsLabel;
    private javax.swing.JLabel noticeLabel;
    private javax.swing.JCheckBox paidCheckBox;
    private javax.swing.JTextField phoneNumberField;
    private javax.swing.JLabel phoneNumberLabel;
    private javax.swing.JPanel staffFormPanel;
    private javax.swing.JLabel startDateLabel;
    private org.jdesktop.swingx.JXDatePicker startDatePicker;
    private javax.swing.JButton submitButton;
    private javax.swing.JTextField timeField;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JSpinner timeSpinner;
    private javax.swing.JTextField totalAmountField;
    private javax.swing.JLabel totalAmountLabel;
    private javax.swing.JTextField vatPercentageField;
    // End of variables declaration//GEN-END:variables
}
