package event_manager.models;

import event_manager.helpers.DBHelper;
import event_manager.helpers.FileHelper;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.table.TableModel;
import org.apache.commons.lang3.ArrayUtils;
import org.javalite.activejdbc.Model;

/**
 *
 * @author uzzaldevkota
 */
public class CompanyConstant extends Model implements AppModel {
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private final String[] columnNames = {
        "id", "storekeeper_cost", "cost_per_labour", "cost_per_vehicle_trip", "staff_expense_percentage",
        "staff_vatto", "revenue_threshold_percentage", "created_at", "updated_at"
    };
    private final String tableName = "company_constants";

    @Override
    public Object getId() {
        return get("id");
    }

    public Object getStorekeeperCostPercentage() {
        return get("storekeeper_cost_percentage");
    }

    public void setStorekeeperCostPercentage(Object cost) {
        set("storekeeper_cost_percentage", cost);
        changeSupport.firePropertyChange("storekeeperCostPercentage", getStorekeeperCostPercentage(), cost);
    }

    public Object getCostPerLabour() {
        return get("cost_per_labour");
    }

    public void setCostPerLabour(Object cost) {
        set("cost_per_labour", cost);
        changeSupport.firePropertyChange("costPerLabour", getCostPerLabour(), cost);
    }

    public Object getCostPerVehicleTrip() {
        return get("cost_per_vehicle_trip");
    }

    public void setCostPerVehicleTrip(Object cost) {
        set("cost_per_vehicle_trip", cost);
        changeSupport.firePropertyChange("costPerVehicleTrip", getCostPerVehicleTrip(), cost);
    }

    public Object getStaffExpensePercentage() {
        return get("staff_expense_percentage");
    }

    public void setStaffExpensePercentage(Object per) {
        set("staff_expense_percentage", per);
        changeSupport.firePropertyChange("staffExpensePercentage", getStaffExpensePercentage(), per);
    }

    public Object getStaffVatto() {
        return get("staff_vatto");
    }

    public void setStaffVatto(Object vatto) {
        set("staff_vatto", vatto);
        changeSupport.firePropertyChange("staffVatto", getStaffVatto(), vatto);
    }

    public Object getRevenueThresholdPercentage() {
        return get("revenue_threshold_percentage");
    }

    public void setRevenueThresholdPercentage(Object per) {
        set("revenue_threshold_percentage", per);
        changeSupport.firePropertyChange("revenueThresholdPercentage", getRevenueThresholdPercentage(), per);
    }

    public Object getCreatedAt() {
        return get("created_at");
    }

    public Object getUpdatedAt() {
        return get("updated_at");
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public TableModel getTableModel(int pageNum) {
        String[] viewableColumns = ArrayUtils.removeElements(columnNames, "created_at");
         return DBHelper.columnsToTableModel(tableName, pageNum, viewableColumns);
    }

    @Override
    public String tableName() {
        return tableName;
    }

    @Override
    public String getShowInfo() {
        String[] replacements = ArrayUtils.removeElements(columnNames, "id", "created_at", "updated_at");
        return FileHelper.getReplacedContents(this, "html/rates.html", replacements);
    }

    public static CompanyConstant getLast() {
        return findFirst("1=? ORDER BY id DESC", "1");
    }
}
