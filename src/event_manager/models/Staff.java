package event_manager.models;

import event_manager.helpers.DBHelper;
import event_manager.helpers.FileHelper;
import event_manager.models.custom_validators.UniquenessValidator;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableModel;
import org.apache.commons.lang3.ArrayUtils;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.Model;

/**
 *
 * @author uzzaldevkota
 */
public class Staff extends Model implements AppModel {
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private final String[] columnNames = {
     "id", "username", "password", "first_name", "last_name", "address", "profile_picture",
     "phone_no", "email", "joined_date", "is_admin", "total_earning", "credit_balance", "created_at", "updated_at"
    };
    private final String tableName = "staffs";
    private String profilePicturePath;

    static {
        validatePresenceOf("username", "password");
        validateWith(new UniquenessValidator("username"));
    }

    @Override
    public Object getId() {
        return get("id");
    }

    public String getProfilePicturePath() {
        return profilePicturePath;
    }

    public void setProfilePicturePath(String profilePicturePath) {
        Object oldProfilePicturePath = this.profilePicturePath;
        this.profilePicturePath = profilePicturePath;
        changeSupport.firePropertyChange("profilePicturePath", oldProfilePicturePath, profilePicturePath);
    }

    public Object getUsername() {
        return get("username");
    }

    public void setUsername(Object username) {
        set("username", username);
        changeSupport.firePropertyChange("username", getUsername(), username);
    }

    public Object getPassword() {
        return get("password");
    }

    public void setPassword(Object password) {
        set("password", password);
        changeSupport.firePropertyChange("password", getPassword(), password);
    }

    public Object getFirstName() {
        return get("first_name");
    }

    public void setFirstName(Object firstName) {
        set("first_name", firstName);
        changeSupport.firePropertyChange("firstName", getFirstName(), firstName);
    }

    public Object getLastName() {
        return get("last_name");
    }

    public void setLastName(Object lastName) {
        set("last_name", lastName);
        changeSupport.firePropertyChange("lastName", getLastName(), lastName);
    }

    public Object getAddress() {
        return get("address");
    }

    public void setAddress(Object address) {
        set("address", address);
        changeSupport.firePropertyChange("address", getAddress(), address);
    }

    public Object getPhoneNo() {
        return get("phone_no");
    }

    public void setPhoneNo(Object phoneNo) {
        set("phone_no", phoneNo);
        changeSupport.firePropertyChange("phoneNo", getPhoneNo(), phoneNo);
    }

    public Object getEmail() {
        return get("email");
    }

    public void setEmail(Object email) {
        set("email", email);
        changeSupport.firePropertyChange("email", getEmail(), email);
    }

    public Object getJoinedDate() {
        return get("joined_date");
    }

    public void setJoinedDate(Object joinedDate) {
        set("joined_date", joinedDate);
        changeSupport.firePropertyChange("joinedDate", getJoinedDate(), joinedDate);
    }

    public Object getIsAdmin() {
        return get("is_admin");
    }

    public void setIsAdmin(Object isAdmin) {
        set("is_admin", isAdmin);
        changeSupport.firePropertyChange("isAdmin", getIsAdmin(), isAdmin);
    }

    public Object getTotalEarning() {
        return get("total_earning");
    }

    public void setTotalEarning(Object totalEarning) {
        set("total_earning", totalEarning);
        changeSupport.firePropertyChange("totalEarning", getTotalEarning(), totalEarning);
    }

    public Object getCreditBalance() {
        return get("credit_balance");
    }

    public void setCreditBalance(Object creditBalance) {
        set("credit_balance", creditBalance);
        changeSupport.firePropertyChange("creditBalance", getCreditBalance(), creditBalance);
    }

    public Object getCreatedAt() {
        return get("created_at");
    }

    public Object getUpdatedAt() {
        return get("updated_at");
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    public boolean isAdmin() {
        return Base.count(tableName, "username=? AND password=? AND is_admin=true", getUsername(), getPassword()) > 0;
    }

    @Override
    public TableModel getTableModel(int pageNum) {
        String[] viewableColumns = ArrayUtils.removeElements(columnNames, "profile_picture", "created_at", "total_earning", "credit_balance");
        return DBHelper.columnsToTableModel(tableName, pageNum, viewableColumns);
    }

    @Override
    public String tableName() {
        return tableName;
    }

    @Override
    public String getShowInfo() {
        String[] replacements = ArrayUtils.removeElements(columnNames, "id", "password", "created_at", "updated_at");
        return FileHelper.getReplacedContents(this, FileHelper.RESOURCES_DIR + "html/profile.html", replacements);
    }

    public static DefaultListModel getListModel(Event event) {
        DBHelper.openConnection();
        DefaultListModel listModel = new DefaultListModel();
        String query = "SELECT * FROM staffs WHERE id NOT IN "
                + "(SELECT staff_id FROM event_staff_transactions WHERE event_id = ?)";
        List<Staff> staffs = Staff.findBySQL(query, event.getId());
        for (Staff staff : staffs) {
            listModel.addElement(staff);
        }
        DBHelper.closeConnection();
        return listModel;
    }

    public static ListCellRenderer getListCellRenderer() {
        return new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component renderer = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (renderer instanceof JLabel && value instanceof Staff) {
                    ((JLabel) renderer).setText(((Staff) value).getUsername().toString());
                }
                return renderer;
            }
        };
    }
}
