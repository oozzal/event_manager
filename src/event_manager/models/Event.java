package event_manager.models;

import event_manager.helpers.DBHelper;
import event_manager.helpers.FileHelper;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableModel;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.Model;

/**
 *
 * @author uzzaldevkota
 */
public class Event extends Model implements AppModel {
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private final String[] columnNames = {
        "id", "client_id", "name", "description", "location", "phone_no",
        "start_date", "end_date", "time", "total_amount", "amount_with_vat", "advance", "no_of_labours",
        "no_of_vehicle_trips", "extra_expense", "commission", "vat_percentage",
        "paid", "created_at", "updated_at"
    };
    private final String tableName = "events";

    @Override
    public Object getId() {
        return get("id");
    }

    public Object getClientId() {
        return get("client_id");
    }

    public void setClientId(Object id) {
        set("client_id", id);
        changeSupport.firePropertyChange("clientId", getClientId(), id);
    }

    public Object getName() {
        return get("name");
    }

    public void setName(Object name) {
        set("name", name);
        changeSupport.firePropertyChange("name", getName(), name);
    }

    public Object getDescription() {
        return get("description");
    }

    public void setDescription(Object desc) {
        set("description", desc);
        changeSupport.firePropertyChange("description", getDescription(), desc);
    }

    public Object getLocation() {
        return get("location");
    }

    public void setLocation(Object loc) {
        set("location", loc);
        changeSupport.firePropertyChange("location", getLocation(), loc);
    }

    public Object getPhoneNo() {
        return get("phone_no");
    }

    public void setPhoneNo(Object no) {
        set("phone_no", no);
        changeSupport.firePropertyChange("phoneNo", getPhoneNo(), no);
    }

    public Object getStartDate() {
        return get("start_date");
    }

    public void setStartDate(Object date) {
        set("start_date", date);
        changeSupport.firePropertyChange("startDate", getStartDate(), date);
    }

    public Object getEndDate() {
        return get("end_date");
    }

    public void setEndDate(Object date) {
        set("end_date", date);
        changeSupport.firePropertyChange("endDate", getEndDate(), date);
    }

    public Object getTime() {
        return get("time");
    }

    public void setTime(Object time) {
        set("time", time);
        changeSupport.firePropertyChange("time", getTime(), time);
    }

    public Object getTotalAmount() {
        return get("total_amount");
    }

    public void setTotalAmount(Object amt) {
        set("total_amount", amt);
        changeSupport.firePropertyChange("totalAmount", getTotalAmount(), amt);
    }

    public Object getAdvance() {
        return get("advance");
    }

    public void setAdvance(Object adv) {
        set("advance", adv);
        changeSupport.firePropertyChange("advance", getAdvance(), adv);
    }

    public Object getNoOfLabours() {
        return get("no_of_labours");
    }

    public void setNoOfLabours(Object labours) {
        set("no_of_labours", labours);
        changeSupport.firePropertyChange("noOfLabours", getNoOfLabours(), labours);
    }

    public Object getNoOfVehicleTrips() {
        return get("no_of_vehicle_trips");
    }

    public void setNoOfVehicleTrips(Object trips) {
        set("no_of_vehicle_trips", trips);
        changeSupport.firePropertyChange("noOfVehicleTrips", getNoOfVehicleTrips(), trips);
    }

    public Object getExtraExpense() {
        return get("extra_expense");
    }

    public void setExtraExpense(Object exp) {
        set("extra_expense", exp);
        changeSupport.firePropertyChange("extra_expense", getExtraExpense(), exp);
    }

    public Object getCommission() {
        return get("commission");
    }

    public void setCommission(Object com) {
        set("commission", com);
        changeSupport.firePropertyChange("commission", getCommission(), com);
    }

    public Object getVatPercentage() {
        return get("vat_percentage");
    }

    public void setVatPercentage(Object per) {
        set("vat_percentage", per);
        changeSupport.firePropertyChange("vatPercentage", getVatPercentage(), per);
    }

    public Object getAmountWithVat() {
        return get("amount_with_vat");
    }

    public void setAmountWithVat(Object amt) {
        set("amount_with_vat", amt);
        changeSupport.firePropertyChange("amountWithVat", getAmountWithVat(), amt);
    }

    public Object getPaid() {
        return get("paid");
    }

    public void setPaid(Object paid) {
        set("paid", paid);
        changeSupport.firePropertyChange("paid", getPaid(), paid);
    }

    public Object getCreatedAt() {
        return get("created_at");
    }

    public Object getUpdatedAt() {
        return get("updated_at");
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public TableModel getTableModel(int pageNum) {
        String[] viewableColumns = ArrayUtils.removeElements(columnNames, "description", "location", "phone_no", "start_date", "end_date",
                "total_amount", "vat_percentage", "time", "created_at");
        TableModel tableModel = DBHelper.columnsToTableModel(tableName, pageNum, viewableColumns);
        if(tableModel.getRowCount() < 1) return tableModel;
        // client_id is at index 1
        int clientId = (Integer) tableModel.getValueAt(0, 1);
        DBHelper.openConnection();
        Client client = Client.findById(clientId);
        for(int i = 0; i < tableModel.getRowCount(); i++) {
            int nextClientId = (Integer) tableModel.getValueAt(i, 1);
            if(nextClientId != clientId) {
                clientId = nextClientId;
                client = Client.findById(clientId);
            }
            tableModel.setValueAt(client.get("name"), i, 1);
        }
        DBHelper.closeConnection();
        return tableModel;
    }

    @Override
    public String tableName() {
        return tableName;
    }

    @Override
    public String getShowInfo() {
        String[] replacements = ArrayUtils.removeElements(columnNames, "id", "created_at", "updated_at");
        return FileHelper.getReplacedContents(this, FileHelper.RESOURCES_DIR + "html/event.html", replacements);
    }

    private double getTotalExpense() {
        CompanyConstant rate = CompanyConstant.getLast();
        double storekeeperCost = rate.getDouble("storekeeper_cost_percentage")/100 * getDouble("total_amount");
        double vehicleCost = rate.getDouble("cost_per_vehicle_trip") * getDouble("no_of_vehicle_trips");
        double labourCost = rate.getDouble("cost_per_labour") * getDouble("no_of_labours");
        double staffVatto = rate.getDouble("staff_vatto");
        double extraExpense = getDouble("extra_expense");
        double commission = getDouble("commission");
        double vat = getDouble("vat_percentage") / 100 * getDouble("total_amount");
        return (storekeeperCost + vehicleCost + labourCost + staffVatto
                 + extraExpense + commission + vat);
    }

    public double getNetAmount() {
        return getDouble("total_amount") - getTotalExpense();
    }
    
    public void addStaffs(Object...staffIds) {
        // Add staff core logic
        String query = "INSERT INTO event_staff_transactions("
                + "event_id, staff_id, username"
                + ") VALUES";
        DBHelper.openConnection();
        List<Staff> staffs = Staff.find("id IN (?)", staffIds);
        List<String> values = new ArrayList<String>();
        for (Staff staff : staffs) {
            String value = "(";
            value += getId() + "," + staff.getId() + ",'" + staff.getUsername() + "'";
            value += ")";
            values.add(value);
        }
        query += StringUtils.join(values, ",");
        Base.exec(query);
        // average_staff_percentage, wage
        long staffCount = Base.count("event_staff_transactions", "event_id=?", getId());
        double avg_staff_per = 100 / staffCount;
        double wage = getNetAmount() / staffCount;
        String sync_query = "UPDATE event_staff_transactions"
                + " SET average_staff_earning_percentage=?, wage=?"
                + " WHERE event_id=?";
        Base.exec(sync_query, avg_staff_per, wage, getId());
        DBHelper.closeConnection();
    }
}
