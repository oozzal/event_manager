package event_manager.models;

import event_manager.helpers.DBHelper;
import event_manager.helpers.FileHelper;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableModel;
import org.apache.commons.lang3.ArrayUtils;
import org.javalite.activejdbc.Model;

/**
 *
 * @author uzzaldevkota
 */
public class Client extends Model implements AppModel {
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private final String[] columnNames = {
        "id", "name", "created_at", "updated_at"
    };
    private final String tableName = "clients";

    @Override
    public Object getId() {
        return get("id");
    }

    public Object getName() {
        return get("name");
    }

    public void setName(Object name) {
        set("name", name);
        changeSupport.firePropertyChange("name", getName(), name);
    }

    public Object getCreatedAt() {
        return get("created_at");
    }

    public Object getUpdatedAt() {
        return get("updated_at");
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public TableModel getTableModel(int pageNum) {
        String[] viewableColumns = ArrayUtils.removeElements(columnNames, "created_at");
         return DBHelper.columnsToTableModel(tableName, pageNum, viewableColumns);
    }

    @Override
    public String tableName() {
        return tableName;
    }

    @Override
    public String getShowInfo() {
        String[] replacements = ArrayUtils.removeElements(columnNames, "id", "created_at", "updated_at");
        return FileHelper.getReplacedContents(this, FileHelper.RESOURCES_DIR + "html/client.html", replacements);
    }

    public static ComboBoxModel getComboBoxModel() {
        DBHelper.openConnection();
        List<Client> clients = Client.findAll();
        Vector<Client> items = new Vector<Client>();
        for (Client client : clients) {
            items.addElement(client);
        }
        DBHelper.closeConnection();
        return new DefaultComboBoxModel(items);
    }
    
    public static ListCellRenderer getListCellRenderer() {
        return new DefaultListCellRenderer(){
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                Component renderer = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (renderer instanceof JLabel && value instanceof Client) {
                    ((JLabel) renderer).setText(((Client) value).getName().toString());
                }
                return renderer;
            }
        };
    }
}
