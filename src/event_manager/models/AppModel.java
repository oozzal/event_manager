package event_manager.models;

import javax.swing.table.TableModel;

/**
 * Interface for application models
 * @author uzzaldevkota
 */
public interface AppModel {    
    /**
     * Builds table model
     * @param pageNum The page number
     * @return TableModel
     */
    public TableModel getTableModel(int pageNum);
    
    /**
     * Gets the table name of the model
     * @return String Table Name
     */
    public String tableName();

    /**
     * Builds the model info to show
     * @return String the model detailed information
     */
    public String getShowInfo();
}
