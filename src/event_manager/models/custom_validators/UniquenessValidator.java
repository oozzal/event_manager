package event_manager.models.custom_validators;

import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.validation.ValidatorAdapter;

/**
 * Validates uniqueness of a model attribute
 *
 * @author uzzaldevkota
 */
public class UniquenessValidator extends ValidatorAdapter {

    private final String attribute;

    public UniquenessValidator(String attribute) {
        this.attribute = attribute;
        setMessage("should be unique");
    }

    @Override
    public void validate(Model m) {
        if(Base.count(Model.getTableName(), attribute + " = ? AND id IS NOT ?", m.get(attribute), m.getId()) > 0) {
            m.addValidator(this, attribute);
        }
    }

}
