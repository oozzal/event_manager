package event_manager.base;

/**
 * Database Setup SQL Queries
 * @author uzzaldevkota
 */
public interface SetupQuery {
    String[] queries = {
        "CREATE TABLE IF NOT EXISTS company_constants("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "storekeeper_cost_percentage REAL DEFAULT 2,"
            + "cost_per_labour INT DEFAULT 600,"
            + "cost_per_vehicle_trip INT DEFAULT 600,"
            + "staff_expense_percentage REAL DEFAULT 25,"
            + "staff_vatto INT DEFAULT 1400,"
            + "revenue_threshold_percentage REAL DEFAULT 60,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        "CREATE TABLE IF NOT EXISTS staffs("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "username VARCHAR(50) UNIQUE,"
            + "password VARCHAR(50),"
            + "first_name VARCHAR(50),"
            + "last_name VARCHAR(50),"
            + "address VARCHAR(100),"
            + "phone_no VARCHAR(20),"
            + "email VARCHAR(50),"
            + "profile_picture VARCHAR(100) NOT NULL DEFAULT 'profile.jpg',"
            + "joined_date DATE,"
            + "is_admin BOOLEAN DEFAULT FALSE,"
            + "total_earning REAL,"
            + "credit_balance REAL,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        "CREATE TABLE IF NOT EXISTS clients("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "name VARCHAR(100),"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
       
        "CREATE TABLE IF NOT EXISTS events("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "client_id INT,"
            + "name VARCHAR(100),"
            + "description VARCHAR(255),"
            + "location VARCHAR(100),"
            + "phone_no VARCHAR(20),"
            + "start_date DATE,"
            + "end_date DATE,"
            + "time TIME,"
            + "total_amount REAL DEFAULT 0,"
            + "advance REAL DEFAULT 0,"
            + "no_of_labours INT DEFAULT 0,"
            + "no_of_vehicle_trips INT DEFAULT 0,"
            + "extra_expense REAL DEFAULT 0,"
            + "commission REAL DEFAULT 0,"
            + "vat_percentage REAL DEFAULT 0,"
            + "amount_with_vat REAL DEFAULT 0,"
            + "paid BOOLEAN DEFAULT FALSE,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        "CREATE TABLE IF NOT EXISTS vehicle_trips("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "event_id INT,"
            + "name VARCHAR(100),"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        "CREATE TABLE IF NOT EXISTS event_staff_transactions("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "event_id INT NOT NULL,"
            + "staff_id INT NOT NULL,"
            + "username VARCHAR(50) NOT NULL,"
            + "average_staff_earning_percentage REAL NOT NULL DEFAULT 0,"
            + "actual_staff_earning_percentage REAL NOT NULL DEFAULT 0,"
            + "wage REAL NOT NULL DEFAULT 0,"
            + "bonus REAL NOT NULL DEFAULT 0,"
            + "total_wage REAL NOT NULL DEFAULT 0,"
            + "paid BOOLEAN DEFAULT FALSE,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        "CREATE TABLE IF NOT EXISTS notifications("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "title VARCHAR(100),"
            + "body VARCHAR(255),"
            + "is_active BOOLEAN DEFAULT FALSE,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        // Every field is dependent on some other model's field
        "CREATE TABLE IF NOT EXISTS expense_summaries("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "event_id INT NOT NULL,"
            + "labour_cost REAL NOT NULL,"
            + "vehicle_cost REAL NOT NULL,"
            + "storekeeper_cost REAL NOT NULL,"
            + "staff_vatto REAL NOT NULL,"
            + "extra_expense REAL NOT NULL,"
            + "commission REAL NOT NULL,"
            + "vat REAL NOT NULL,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        "CREATE TABLE IF NOT EXISTS staff_summaries("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "staff_id INT NOT NULL,"
            + "average_earning REAL NOT NULL,"
            + "average_presence REAL NOT NULL,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        "CREATE TABLE IF NOT EXISTS event_analyses("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "event_id INT NOT NULL,"
            + "is_total_wage_equal BOOLEAN DEFAULT FALSE,"
            + "expected_staff_expense REAL DEFAULT 0,"
            + "actual_staff_expense REAL NOT NULL,"
            + "staff_expense_exceeded_by REAL NOT NULL,"
            + "is_revenue_below_threshold BOOLEAN DEFAULT FALSE,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)",
        
        "CREATE TABLE IF NOT EXISTS event_summaries("
            + "id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
            + "event_id INT NOT NULL,"
            + "total_amount REAL NOT NULL,"
            + "total_expense REAL NOT NULL,"
            + "net_amount REAL NOT NULL,"
            + "bonus REAL NOT NULL,"
            + "staff_expense REAL NOT NULL,"
            + "average_staff_percentage REAL NOT NULL,"
            + "total_staff_expense REAL NOT NULL,"
            + "revenue_to_company REAL NOT NULL,"
            + "company_revenue_percentage REAL NOT NULL,"
            + "company_expense_percentage REAL NOT NULL,"
            + "created_at TIMESTAMP, updated_at TIMESTAMP)"
    };
}
