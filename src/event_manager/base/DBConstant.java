package event_manager.base;

/**
 * Database settings and constants
 * @author uzzaldevkota
 */
public interface DBConstant {
    public static final String DRIVER = "org.h2.Driver";
    public static final String URL = "jdbc:h2:event_manager";
    public static final String USERNAME = "iss";
    public static final String PASSWORD = "iss0977";
}
