package event_manager.base;

import java.awt.Color;
import java.util.Calendar;

/**
 * Application settings and constants
 * 
 * @author uzzaldevkota
 */
public interface AppConstant {
    public static final String companyShortName = "ISS";
    public static final String appName = companyShortName + " Event Management System";
    public static final String companyName = "International Sound Service";
    public static final String copyrightInfo = "Copyright © "
            + Calendar.getInstance().get(Calendar.YEAR) + ", " + companyName + ".";
    public static final String FONT_NAME = "Lucida Grande";
    public static final int HEADING_SIZE = 20;
    public static final String EMAIL_USERNAME = "test.oozzal@gmail.com";
    public static final String EMAIL_PASSWORD = "testoozzal";
    public static final Color success_color = new Color(0, 145, 0);
    public static final Color failure_color = new Color(255, 0, 0);
}
