package event_manager.validators;

import org.jdesktop.beansbinding.Validator;

/**
 * Validates string is not empty
 * 
 * @author uzzaldevkota
 */
public class MinimumLengthValidator extends RequiredStringValidator {

    private int length;
    
    public MinimumLengthValidator(int len) {
        length = len;
    }

    public void setLength(int len) {
        length = len;
    }

    @Override
    public Validator.Result validate(String arg) {
        if (arg.length() < length) {
            return new Validator.Result(null, "Too Short");
        }
        return null;
    }
}
