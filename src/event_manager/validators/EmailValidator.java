package event_manager.validators;

import org.jdesktop.beansbinding.Validator;
import java.util.regex.Pattern;

/**
 * Validates email
 * 
 * @author uzzaldevkota
 */
public class EmailValidator extends Validator<String> {
    private static final String EMAIL_PATTERN = 
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Override
    public Validator.Result validate(String arg) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        if (!pattern.matcher(arg).matches()) {
            return new Validator.Result(null, "Invalid e-mail");
        }
        return null;
    }
}
