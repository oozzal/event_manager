package event_manager.validators;

import org.jdesktop.beansbinding.Validator;

/**
 * Validates number
 * 
 * @author uzzaldevkota
 */
public class NumberValidator extends Validator<String> {

    @Override
    public Validator.Result validate(String arg) {
        try {
            Float.parseFloat(arg);
        } catch (NumberFormatException e) {
            return new Validator.Result(null, "Not a number");
        }
        return null;    
    }

}
