package event_manager.validators;

import org.jdesktop.beansbinding.Validator;
import java.util.regex.Pattern;

/**
 * Validates phone number
 * 
 * @author uzzaldevkota
 */
public class PhoneNumberValidator extends Validator<String> {
    private static final String PHONE_NO_PATTERN = "\\+?\\d{9,14}$";

    @Override
    public Validator.Result validate(String arg) {
        Pattern pattern = Pattern.compile(PHONE_NO_PATTERN);
        if (!pattern.matcher(arg).matches()) {
            return new Validator.Result(null, "Invalid Phone Number");
        }
        return null;
    }
}
