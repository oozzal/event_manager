package event_manager.validators;

import org.jdesktop.beansbinding.Validator;

/**
 * Validates string is not empty
 * 
 * @author uzzaldevkota
 */
public class RequiredStringValidator extends Validator<String> {

    @Override
    public Validator.Result validate(String arg) {
        if ((arg == null) || (arg.length() == 0)) {
            return new Validator.Result(null, "Empty value");
        }
        return null;
    }
}
