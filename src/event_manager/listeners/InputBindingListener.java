package event_manager.listeners;

import event_manager.base.AppConstant;
import javax.swing.JTextField;
import org.jdesktop.beansbinding.AbstractBindingListener;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Binding.SyncFailure;

/**
 * Binding listener used to change background of input fields
 * 
 * @author uzzaldevkota
 */
public class InputBindingListener extends AbstractBindingListener {
    private JTextField inputLabel;

    public InputBindingListener(JTextField inputLabel) {
        if (inputLabel == null) throw new IllegalArgumentException();
        this.inputLabel = inputLabel;
    }

    @Override
    public void syncFailed(Binding binding, SyncFailure fail) {
        inputLabel.setForeground(AppConstant.failure_color);
    }

    @Override
    public void synced(Binding binding) {
        inputLabel.setForeground(AppConstant.success_color);
    }

}
