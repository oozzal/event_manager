package event_manager.helpers;

/**
 *
 * @author uzzaldevkota
 */
public class HtmlHelper {

    /**
     * Returns a row to display
     * @param content The content of the row
     * @return String The built row
     */
    public static String row(Object content) {
        return wrap(content, "p");
    }
    
    /**
     * Wraps a content in a tag
     * @param content The string content
     * @param tag The type of tag
     * @return String
     */
    public static String wrap(Object content, String tag) {
        return "<"+tag+">" + content + "</"+tag+">";
    }

    /**
     * Adds a line break
     * @param content
     * @return 
     */
    public static String lineBreak(Object content) {
        return content + "<br>";
    }
}
