package event_manager.helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FilenameUtils;
import org.javalite.activejdbc.Model;

/**
 *
 * @author uzzaldevkota
 */
public class FileHelper {
    public static final String RESOURCES_DIR = "resources/";
    public static final String IMAGES_DIR = RESOURCES_DIR+"images/";
    public static final String PROFILE_PIC_DIR = IMAGES_DIR+"profile_pictures/";
    // Limit upload size to 2 MB
    public static final long UPLOAD_LIMIT = 2;
    private static final Logger logger = Logger.getLogger(FileHelper.class.getName());
    
    public static String absolutePathToLocalFilename(String absolutePath, String localName) {
        String fileName = new File(absolutePath).getName();
        String extension = FilenameUtils.getExtension(fileName);
        return localName + "." + extension;
    }
    
    public static long fileSizeInMB(File file) {
        return file.length() / (1024 * 1024);
    }

    public static String getContents(String filePath) {
        String content = "";
        try {
            content = new Scanner(new File(filePath)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return content;
    }

    public static String getReplacedContents(Model m, String filePath, String...replacements) {
        String contents = getContents(filePath);
        for (String replacement : replacements) {
            String value;
            try {
                value = m.get(replacement).toString();
            } catch(Exception ex) {
                value = "N/A";
            }
            contents = contents.replace("{{"+replacement+"}}", value);
        }
        return contents;
    }
}
