package event_manager.helpers;

import event_manager.base.AppConstant;
import javax.swing.JOptionPane;

/**
 * Helper for views
 * 
 * @author uzzaldevkota
 */
public class ViewHelper {
    public static final int YES = 0;
    public static final int NO = 1;
    public static final int CANCEL = 2;
    public static final int LIST_EVENT = 3;
    public static final int LIST_STAFF = 4;
    public static final int LIST_CLIENT = 5;
    public static final int TAB_LIMIT = 3;

    public static void showDialog(String msg) {
        JOptionPane.showMessageDialog(null, msg);
    }

    public static int showConfirmDialog(String msg) {
        return JOptionPane.showConfirmDialog(null, msg);
    }

    public static String getTitle(String simpleName) {
        return AppConstant.companyShortName + " - " + simpleName;
    }
    
    public static void showAbout() {
        String aboutMsg = AppConstant.appName + "\n"
                + AppConstant.copyrightInfo;
        showDialog(aboutMsg);
    }

}
