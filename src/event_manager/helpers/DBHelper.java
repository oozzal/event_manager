package event_manager.helpers;

import event_manager.base.DBConstant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.commons.lang3.StringUtils;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.Model;

/**
 * Database Helper
 * @author uzzaldevkota
 */
public class DBHelper {
    private static final int PER_PAGE = 15;

    public static void openConnection() {
        if(!Base.hasConnection()) {
            Base.open(DBConstant.DRIVER, DBConstant.URL, DBConstant.USERNAME, DBConstant.PASSWORD);
        }
    }
    
    public static void closeConnection() {
        if(Base.hasConnection()) {
            Base.close();
        }
    }

    public static String getNiceErrors(Model model) {
        String output = "";
        Map<String, String> errors = model.errors();
        for (Map.Entry<String, String> error : errors.entrySet()) {
            output += error.getKey() + " => " + error.getValue();
        }
        return output;
    }

    public static TableModel columnsToTableModel(String tableName, int pageNum, String...columns) {
        Vector<String> columnNames = new Vector<String>();
        
        String query = "SELECT ";
        query += StringUtils.join(columns, ", ");
        query += " FROM " + tableName + " LIMIT " + PER_PAGE + " OFFSET " + pageNum * PER_PAGE;

        openConnection();

        List<Map> results = Base.findAll(query);
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

        Collections.addAll(columnNames, columns);

        for (Map row : results) {
            Vector<Object> newRow = new Vector<Object>();
            for (String columnName : columnNames) {
                newRow.addElement(row.get(columnName));
            }
            rows.addElement(newRow);
        }

        closeConnection();

        return new DefaultTableModel(rows, columnNames) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }

}
