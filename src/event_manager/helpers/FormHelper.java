package event_manager.helpers;

import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;

/**
 *
 * @author uzzaldevkota
 */
public class FormHelper {
    /**
     * Generates a bean binding
     * @param source the form
     * @param model model object name
     * @param sourceProperty property of source
     * @param target target to bind to
     * @param targetProperty property of target
     * @return Binding
     */
    public static Binding createBinding(Object source, String model, String sourceProperty, Object target, String targetProperty) {
        return Bindings.createAutoBinding(AutoBinding.UpdateStrategy.READ_WRITE,
                source, ELProperty.create("${"+model+"." +sourceProperty+"}"), target, BeanProperty.create(targetProperty), sourceProperty);
    }

}
