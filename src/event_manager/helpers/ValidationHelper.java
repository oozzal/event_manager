package event_manager.helpers;

import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;

/**
 *
 * @author uzzaldevkota
 */
public class ValidationHelper {

    public static boolean validate(BindingGroup bindingGroup) {
        boolean isValid = true;
        String errors = "";
        for (Binding binding : bindingGroup.getBindings()) {
            Binding.SyncFailure failure = binding.save();
            if(failure != null) {
                errors += binding.getName() + " => " + failure.getValidationResult().getDescription() + "\n";
                isValid = false;
            }
        }
        if(!isValid) {
            ViewHelper.showDialog("Please correct following errors:\n\n" + errors);
        }
        return isValid;
    }
}
