package event_manager;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.jgoodies.looks.plastic.theme.ExperienceRoyale;
import event_manager.base.SetupQuery;
import event_manager.helpers.DBHelper;
import event_manager.models.Client;
import event_manager.models.CompanyConstant;
import event_manager.models.Event;
import event_manager.models.Staff;
import event_manager.views.Login;
import java.util.Date;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.javalite.activejdbc.Base;

/**
 * Start of the event manager
 *
 * @author uzzaldevkota
 */
public class EventManager {

    private static void ensureDatabase() {
        DBHelper.openConnection();
        try {
            if(Base.count("staffs") > 0) {
                DBHelper.closeConnection();
                return;
            }
        } catch (Exception e) {
            System.out.println("Setting up Database");
            for (String query : SetupQuery.queries) {
                Base.exec(query);
            }
            Staff.createIt("username", "oozzal", "password", "oozzal", "is_admin", true, "joined_date", new Date());
            CompanyConstant.createIt();
            Client.createIt("name", "Dummy Client");
            Event.createIt("client_id", 1, "name", "Dummy Event", "location", "Dummy Location",
                "description", "Dummy Description", "phone_no", "9808640958", "total_amount", "100000");
        }
        DBHelper.closeConnection();
    }

    /**
     * @param args the command line arguments
     * @throws javax.swing.UnsupportedLookAndFeelException
     */
    public static void main(String[] args) throws UnsupportedLookAndFeelException {
        LookAndFeel lf;
        Plastic3DLookAndFeel.setPlasticTheme(new ExperienceRoyale());
        lf = new Plastic3DLookAndFeel();
        UIManager.setLookAndFeel(lf);
        ensureDatabase();
        DBHelper.openConnection();
        Event evt = Event.findById(1);
        System.out.println("Net Amount: " + evt.getNetAmount());
//        new Login(new Staff()).setVisible(true);
        DBHelper.closeConnection();
//        Staff staff = Staff.findById(1);
//        new StaffForm(staff).setVisible(true);
//        new StaffForm(new Staff()).setVisible(true);
//        DBHelper.openConnection();
//        List<Map> results = Base.findAll("SELECT * FROM users");
//        for (Map map : results) {
//            for (Object object : map.keySet()) {
//                System.out.println(object);
//            }
//        }
//        DBHelper.closeConnection();
//        try {
//            DBHelper.openConnection();
//            Base.connection().setAutoCommit(false);
////            Base.exec("DROP TABLE users");
//            Base.exec("CREATE TABLE IF NOT EXISTS users(id INT PRIMARY KEY AUTO_INCREMENT, username VARCHAR(50) UNIQUE, password VARCHAR(50), is_admin BOOLEAN, created_at DATETIME, updated_at DATETIME)");
////            Staff u = Staff.findById(1);
////            System.out.println(u.get("password"));
//            Staff u = Staff.create("username", "sadixa", "password", "nanu", "is_admin", false);
//            if(!u.save()) {
//                Map<String, String> errors = u.errors();
//                System.out.println("Errors:");
//                for (Map.Entry<String, String> error : errors.entrySet()) {
//                    System.out.println(error.getKey() + " => " + error.getValue());
//                }
//            } else {
//                System.out.println("Staff saved!");
//            }
//            Base.commitTransaction();
//        } catch (SQLException e) {
//            Base.rollbackTransaction();
//        } finally {
//            DBHelper.closeConnection();
//        }
    }

}
